package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.insight.mobbicheck.R;

import java.util.ArrayList;
import java.util.List;

import support.Health_Tips_Card;
import support.Medical_Terms_Card;

/**
 * Created by Terraformer on 2/27/2015.
 */
public class Health_Tips_Adapter extends ArrayAdapter<Health_Tips_Card> {


    private List<Health_Tips_Card> healthTipsCardList = new ArrayList<Health_Tips_Card>();
    private static final String TAG = "Health_Tips_Adapter";
    Context context;

    public Health_Tips_Adapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }


    static class ViewHolder{
        //ImageView header_image;
        TextView title_text;

    }

    @Override
    public void add(Health_Tips_Card object) {
        healthTipsCardList.add(object);
        super.add(object);

    }

    @Override
    public int getCount() {
        return this.healthTipsCardList.size();
    }

    @Override
    public Health_Tips_Card getItem(int index) {
        return this.healthTipsCardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.health_list_details, parent, false);
            viewHolder = new ViewHolder();
            //viewHolder.header_image = (ImageView) row.findViewById(R.id.card_image);
            viewHolder.title_text = (TextView) row.findViewById(R.id.health_title);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)row.getTag();
        }
        Health_Tips_Card health_tips_card = getItem(position);


         //viewHolder.header_image.setImageResource(card.getImage_header());
        Log.e(Health_Tips_Adapter.class.getSimpleName(),health_tips_card.getTip());
        String holdTips = health_tips_card.getTip();

        viewHolder.title_text.setText(holdTips);
        return row;
    }


    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }


}
