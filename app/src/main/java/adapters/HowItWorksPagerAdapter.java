package adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.insight.mobbicheck.R;

import org.w3c.dom.Text;

public class HowItWorksPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    String[] titles;
    String[] texts;
    int[] images;

    public HowItWorksPagerAdapter(Context context, String[] titles, String[] texts, int[] images) {
        this.context = context;
        this.titles = titles;
        this.texts = texts;
        this.images = images;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    public float getPageWidth(int position) {
        return 0.7f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ImageView imgflag;
        TextView titleText,textView;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.how_it_works, container, false);

        titleText = (TextView)itemView.findViewById(R.id.title);
        textView = (TextView)itemView.findViewById(R.id.text);

        // Locate the ImageView in viewpager_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.flag_image);
        // Capture position and set to the ImageView
        textView.setText(texts[position]);
        titleText.setText(titles[position]);
        imgflag.setImageResource(images[position]);

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}