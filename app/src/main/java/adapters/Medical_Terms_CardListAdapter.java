package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
//import android.widget.ImageView;
import android.widget.TextView;

import com.insight.mobbicheck.R;

import java.util.ArrayList;
import java.util.List;

import support.Medical_Terms_Card;

/**
 * Created by Terraformer on 2/26/2015.
 */
public class Medical_Terms_CardListAdapter extends ArrayAdapter<Medical_Terms_Card> {

    private List<Medical_Terms_Card> medicalTermsCardList = new ArrayList<Medical_Terms_Card>();
    private static final String TAG = "CardArrayAdapter";

    public Medical_Terms_CardListAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    @Override
    public void add(Medical_Terms_Card object) {
        medicalTermsCardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.medicalTermsCardList.size();
    }

    @Override
    public Medical_Terms_Card getItem(int index) {
        return this.medicalTermsCardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.card_details, parent, false);
            viewHolder = new ViewHolder();
            //viewHolder.header_image = (ImageView) row.findViewById(R.id.card_image);
            viewHolder.title_text = (TextView) row.findViewById(R.id.card_title);
            viewHolder.content_text = (TextView)row.findViewById(R.id.card_content);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)row.getTag();
        }
        Medical_Terms_Card medicalTermsCard = getItem(position);
       // viewHolder.header_image.setImageResource(card.getImage_header());
        viewHolder.title_text.setText(medicalTermsCard.getNews_topic());
        viewHolder.content_text.setText(medicalTermsCard.getNews_content());
        return row;
    }

    static class ViewHolder{
        //ImageView header_image;
        TextView title_text;
        TextView content_text;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
