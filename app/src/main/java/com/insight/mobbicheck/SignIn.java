package com.insight.mobbicheck;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class SignIn extends ActionBarActivity {

    EditText user , pass;
    Button incoming;
    ActionBar actionBar;
    TextView signInText;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        user = (EditText)findViewById(R.id.etUserName);
        pass = (EditText)findViewById(R.id.etPass);
        incoming = (Button)findViewById(R.id.btnSignIn);
        signInText = (TextView)findViewById(R.id.signIntext);
        incoming.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                String username = user.getText().toString();
                String userpass = pass.getText().toString();

                if( null == username || username.trim().length() < 6 ){
                    user.setError(" Please enter your username. ( Username should be above six characters ) ");
                    user.requestFocus();
                }
                else if(( null == userpass || userpass.trim().length() <= 5 ) ){
                    pass.setError("Please enter your password. Password should be more than five characters");
                    pass.requestFocus();
                } else {
                    toast("Signed In as " + username);
                    Intent intent = new Intent(SignIn.this , DashBoard.class);
                    startActivity(intent);

                    toast(username);
                }


            }
        });
    }

    public void toast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
