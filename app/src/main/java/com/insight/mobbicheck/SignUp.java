package com.insight.mobbicheck;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class SignUp extends ActionBarActivity {

        String name, email, password;
        EditText user , pass , mail;
        Context context;
        Button signUp;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CheckBox rememberMe;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sign_up);

            user = (EditText)findViewById(R.id.edtUserName);
            pass = (EditText)findViewById(R.id.edtPass);
            mail = (EditText)findViewById(R.id.edtmail);
            signUp = (Button)findViewById(R.id.btnSingUp);
            rememberMe = (CheckBox)findViewById(R.id.remember_me);

            signUp.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO

                    String username = user.getText().toString();
                    String userpass = pass.getText().toString();
                    String usermail = mail.getText().toString();

                    if( null == username || username.trim().length() == 0){
                        user.setError("please enter your username");
                        user.requestFocus();
                    }
                    else if( null == userpass || userpass.trim().length() == 0){
                        pass.setError("Please enter your password");
                        pass.requestFocus();
                    } else if( null == usermail || usermail.trim().length() == 0){
                        pass.setError("Please enter your mail address");
                        pass.requestFocus();
                    }
                    else if( !matchesPattern(usermail)){
                        mail.setError("E-mail address you entered is invalid! ");
                        mail.requestFocus();
                    }
                    else{
                        //do nothing
                        toast("Signing You in as " + username);

                        email = usermail;
                        name = username;
                        password = userpass;


                        Intent intent = new Intent( SignUp.this , DashBoard.class);
                        startActivity(intent);
                        //toast("Hello world");
                    }
                }
            });

        }


    public boolean matchesPattern(String tester)
    {
       return tester.matches(emailPattern);
    }

    public void toast (String message)
    {

        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager  = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
