package com.insight.mobbicheck;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import adapters.HowItWorksPagerAdapter;


public class MainActivity extends ActionBarActivity {

    String [] titles,texts;
    int[] imgs;
    ViewPager viewPager;
    PagerAdapter adapter;
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titles = new String[]{"Who we are" , "What we do" , "How we do it" , "Begin"};
        texts = new String[] {"We are team MobbiCheck, a Mobile-Health based start-up. " , " We write code, we are humans , we want to save lives in any way we can" , "We have created a mobile platform and a bridge to connect our clients(you probably) to Medical Practitioners. Thereby reducing waiting hours or queues in Hospitals " , "Ready to explore?"};
        imgs = new int[]{R.drawable.add, R.drawable.img , R.drawable.brush , R.drawable.begin};

        layout = (LinearLayout)findViewById(R.id.llDots);

        adapter = new HowItWorksPagerAdapter(MainActivity.this,titles,texts,imgs);

        viewPager = (ViewPager)findViewById(R.id.pager);
        viewPager.setAdapter(adapter);

        for (int i = 0; i < adapter.getCount(); i++)
        {
            ImageButton imgDot = new ImageButton(this);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.dot_selector);
            imgDot.setBackgroundResource(0);
            imgDot.setPadding(5, 5, 5, 5);
            LayoutParams params = new LayoutParams(20, 20);
            imgDot.setLayoutParams(params);
            if(i == 0) {
                imgDot.setSelected(true);
            }
            layout.addView(imgDot);
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {

                for (int i = 0; i < adapter.getCount(); i++)
                {
                    if(i != pos)
                    {
                        ((ImageView)layout.findViewWithTag(i)).setSelected(false);
                    }
                }
                ((ImageView)layout.findViewWithTag(pos)).setSelected(true);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


}
