package support;

/**
 * Created by Terraformer on 2/27/2015.
 */
public class Health_Tips_Card {

    String tip;

    public Health_Tips_Card(String tip){
        this.tip = tip;
    }

    public String  getTip(){
        return tip;
    }
}
