package support;

/**
 * Created by Terraformer on 2/26/2015.
 */
public class Medical_Terms_Card {

    //int image_header;
    String news_topic;
    String news_content;

    public Medical_Terms_Card(String news_topic, String news_content){
        //this.image_header = image_header;
        this.news_topic = news_topic;
        this.news_content = news_content;
    }

    /*public int getImage_header(){
        return image_header;
    }*/

    public String getNews_topic(){
        return news_topic;
    }

    public String getNews_content(){
        return news_content;
    }
}
