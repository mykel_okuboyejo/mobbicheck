package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insight.mobbicheck.R;

/**
 * Created by Terraformer on 2/23/2015.
 */
public class Complaint extends Fragment{


    TextView textView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complaints ,container , false);

        textView = (TextView)view.findViewById(R.id.complaints_texts);
        textView.setText("Quick Brown fox Jumps Over a Lazy Sheep");
        return view;
    }
}
