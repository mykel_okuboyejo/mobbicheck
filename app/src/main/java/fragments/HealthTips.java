package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.insight.mobbicheck.R;

import adapters.Health_Tips_Adapter;
import support.Health_Tips_Card;

/**
 * Created by Terraformer on 2/23/2015.
 */
public class HealthTips extends Fragment{

    ListView health_tips_list;
    Health_Tips_Card health_tips;
    Health_Tips_Adapter healthTipsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.health_tips , container, false);

        health_tips_list = (ListView)rootView.findViewById(R.id.health_title_listview);
        String [] health_tips_headers = getResources().getStringArray(R.array.health_tip_items);

        health_tips_list.addHeaderView(new View(getActivity()));
        health_tips_list.addFooterView(new View(getActivity()));

        healthTipsAdapter = new Health_Tips_Adapter(getActivity() , R.layout.health_list_details);

        for(int i = 0; i < health_tips_headers.length; i++){

            health_tips = new Health_Tips_Card(health_tips_headers[i]);
            healthTipsAdapter.add(health_tips);
        }

        health_tips_list.setAdapter(healthTipsAdapter);

        return rootView;
    }
}
