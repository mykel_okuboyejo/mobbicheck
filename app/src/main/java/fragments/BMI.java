package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.insight.mobbicheck.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;
import java.util.List;

import uk.me.lewisdeane.ldialogs.CustomDialog;


public class BMI extends Fragment implements View.OnClickListener{

    EditText age_field,height_field,weight_field;
    //Spinner sex_spinner,height_spinner,weight_spinner;
    List<String> sex_list;//,weight_list,height_list;
    String temp_string_array[];
    ArrayAdapter<String> adapter;
    TextView age;
    int age_value;
    //int bmi_of_user = 0;
    Button calculate_bmi;

    String sex,height,weight;//needed****

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bmi , container , false);

        //age_field = (EditText)rootView.findViewById(R.id.age_text_field);
//        height_field = (EditText)rootView.findViewById(R.id.height_text_field);
//        weight_field = (EditText)rootView.findViewById(R.id.weight_text_field);
          age = (TextView)rootView.findViewById(R.id.ageText);

        final DiscreteSeekBar dsb = (DiscreteSeekBar)rootView.findViewById(R.id.age_seek_bar);
        dsb.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
                return value * 10;
            }
        });

        dsb.setOnProgressChangeListener( new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                age_value = dsb.getProgress();

                age.setText("Age: " + age_value );
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });




        //spinners
        //sex_spinner = (Spinner)rootView.findViewById(R.id.sex_spinner);
      /**  height_spinner = (Spinner)rootView.findViewById(R.id.height_spinner);
        weight_spinner = (Spinner)rootView.findViewById(R.id.weight_spinner);**/

        calculate_bmi = (Button)rootView.findViewById(R.id.bmi_button);

        //lists
        sex_list = new ArrayList<String>();
        /**height_list = new ArrayList<String>();
        weight_list = new ArrayList<String>();**/

        temp_string_array = getResources().getStringArray(R.array.sex_list);
        for(int i = 0; i < temp_string_array.length; i++){
            sex_list.add(temp_string_array[i]);
        }

        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, sex_list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //sex_spinner.setAdapter(adapter);

        /*sex_spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sex = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });**/

        calculate_bmi.setOnClickListener(this);



        return rootView;
    }

    @Override
    public void onClick(View v) {

        CustomDialog.Builder builder = new CustomDialog.Builder(getActivity() , "Your Body-Mass Index" , "OK");
        builder.content("Your BMI is 20.9. This is normal for age." + "\n" + "The Ideal BMI for your age is within the range of 19 to 24");
        CustomDialog dialog = builder.build();
        dialog.show();
    }
}
