package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insight.mobbicheck.R;

/**
 * Created by Terraformer on 2/23/2015.
 */
public class DoctorsNearby extends Fragment {

    TextView text;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.doctors_nearby, container , false);

        text =(TextView)rootView.findViewById(R.id.doctors_text);


        return rootView;
    }
}
